# Telegram Time Analyzer

This package will enable you to analyze the messages that you sent to a specific user.

## Setup

Step one:
```bash
pip install -r requirements.txt
```
Step two:

Create a file named **api.config** in which there are two lines. First line contains your `api_id` and second line contains your `api_hash`. To obtain these values visit [https://my.telegram.org](https://my.telegram.org).

## Usage

Run ```extract_time_data.py``` and it will ask you the username for which you want to draw the chart.