from telethon import TelegramClient, events, sync
from datetime import timezone
import numpy as np
from draw import draw

with open('api.config', 'r') as config_file:
    lines = config_file.readlines()
    api_id = int(lines[0])
    api_hash = lines[1]

client = TelegramClient('session_name', api_id, api_hash)
client.start()
print("Client started!")
my_id = client.get_me().id

username = input("Enter username (or telegram id as a number) e.g.: mohammad or 81273618\n")
if username.isnumeric():
	username = int(username)
message_times = list()
total_msgs = client.get_messages(username).total

i = 0
for message in client.iter_messages(username):
    # if i == 10:
    #     break
    message_time = message.date.replace(tzinfo=timezone.utc).astimezone(tz=None)
    message_times.append([message_time, message.sender.id == my_id])
    i += 1
    print(" Message " + str(i) + " of " + str(total_msgs), end="\r")
print('')
file_path = str(username) + "_times.npy"
np.save(file_path, np.array(message_times))
draw(file_path, True)